#!/usr/bin/bash
h=`python -c "import random; print(random.uniform(0.0,1.0))"`
s=1.0
v=0.15
rgb=`python -c "import colorsys; print(' '.join([str(int(x*255)) for x in colorsys.hsv_to_rgb($h,$s,$v)]))"`
rgb=( $rgb )
pref="\033]11;"
color=`printf \#%02X%02X%02X ${rgb[0]} ${rgb[1]} ${rgb[2]}`
suff="\007"
echo -ne $pref$color$suff
# echo $color
