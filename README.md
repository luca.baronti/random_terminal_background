# Random Terminal Background Changer

Script that set the background colour randomly each time a bash terminal is created.

Usually multiple terminals are opened when different activities need to be performed at the same time.
This script is useful to identify at glance different terminals even after hours that have been opened.

The color is chosen randomly but it is always of a dark tone, to not impact the terminal readibility.

The brighness of the color can be tuned with the value variable $`v\in[0,1]`$ in the file *background_color_changer.sh*. The lower the value, the darker is the colour.

## Dependencies
This script requires Python (tested on 3.x probably works fine on 2.x) and the libraries *random* and *colorsys* that should be already present in the python package.

## Installation

Just clone this repository and run the makefile
```
$ make install
```
to install the script. Once installed, all the newly created terminals should change the color automatically.

To unistall the script just remove the relative lines (that start and end with Random Terminal Background Changer) from the file *~/.bashrc* in your home directory. After that, all the newly created terminals should have their default color.